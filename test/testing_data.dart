import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:multiprovider_analytics/src/models/analytic.dart';
import 'package:multiprovider_analytics/src/models/analytics_bloc_event.dart';

class Evento {}

class EventoPre extends Evento with AnalyticsPreBlocEvent<Estado> {
  final String param;

  EventoPre(this.param);

  @override
  List<TestAnalytic> preAnalytics(Estado preState) {
    return [
      TestAnalytic(id: 1, name: 'EventoPre:preState:${preState.payload}'),
      TestAnalytic(id: 2, name: 'EventoPre:param:$param'),
    ];
  }
}

class EventoPost extends Evento with AnalyticsPostBlocEvent<Estado> {
  EventoPost(this.paramConstructor);

  @override
  FutureOr<void> inBlocSetUp({String? paramInBloc}) {
    if (paramInBloc != null) {
      this.paramInBloc = paramInBloc;
    }
    super.inBlocSetUp();
  }

  final String paramConstructor;
  late final String paramInBloc;

  @override
  List<TestAnalytic> postAnalytics(Estado preState, SubEstado postState) {
    return [
      TestAnalytic(id: 1, name: 'EventoPost:preState:${preState.payload}'),
      TestAnalytic(
          id: 2, name: 'EventoPost:paramConstructor:$paramConstructor'),
      TestAnalytic(id: 3, name: 'EventoPost:paramInBloc:$paramInBloc'),
      TestAnalytic(
          id: 4,
          name:
              'EventoPost:postState:${postState.payload}:${postState.subPayload}'),
    ];
  }
}

class Estado extends Equatable {
  final String payload;

  Estado({required this.payload});

  @override
  List<Object?> get props => [payload];
}

class SubEstado extends Estado {
  final String subPayload;

  SubEstado({required super.payload, required this.subPayload});

  @override
  List<Object?> get props => [payload, subPayload];
}

class TestAnalytic extends Analytic {
  final int id;
  final String name;

  TestAnalytic({required this.id, required this.name});

  static List<String> trackings = [];

  @override
  void trackEvent() {
    trackings.add('TestAnalytic: $name');
  }

  @override
  String toJson() => '{"testAnalytic": "$name"}';
}

class BlocTestWellMade extends Bloc<Evento, Estado> {
  BlocTestWellMade() : super(Estado(payload: 'initial')) {
    on<EventoPre>(_onPre);
    on<EventoPost>(_onPost);
  }

  FutureOr<void> _onPre(EventoPre event, Emitter<Estado> emit) {
    emit(Estado(payload: 'onPre'));
    emit(Estado(payload: 'onPre2'));
  }

  FutureOr<void> _onPost(EventoPost event, Emitter<Estado> emit) {
    event.inBlocSetUp(paramInBloc: 'paramInBloc');
    emit(SubEstado(payload: 'onPostPayload', subPayload: 'onPostSubPayload'));
    emit(SubEstado(payload: 'onPostPayload2', subPayload: 'onPostSubPayload2'));
  }
}

class BlocTestWithMissingInBlocSetUpCall extends Bloc<Evento, Estado> {
  BlocTestWithMissingInBlocSetUpCall() : super(Estado(payload: 'initial')) {
    on<EventoPost>(_onPost);
  }

  FutureOr<void> _onPost(EventoPost event, Emitter<Estado> emit) {
    emit(SubEstado(payload: 'onPostPayload', subPayload: 'onPostSubPayload'));
    emit(SubEstado(payload: 'onPostPayload2', subPayload: 'onPostSubPayload2'));
  }
}

class BlocTestWithMissingInBlocSetUpParameters extends Bloc<Evento, Estado> {
  BlocTestWithMissingInBlocSetUpParameters()
      : super(Estado(payload: 'initial')) {
    on<EventoPost>(_onPost);
  }

  FutureOr<void> _onPost(EventoPost event, Emitter<Estado> emit) {
    event.inBlocSetUp();
    emit(SubEstado(payload: 'onPostPayload', subPayload: 'onPostSubPayload'));
  }
}
