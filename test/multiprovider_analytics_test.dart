import 'dart:async';

import 'package:bloc_test/bloc_test.dart';
import 'package:bloc/bloc.dart';
import 'package:multiprovider_analytics/src/exceptions.dart';
import 'package:multiprovider_analytics/src/multiprovider_analytics_bloc_observer.dart';
import 'package:test/test.dart';

import 'testing_data.dart';

void main() {
  group('Analytics sending', () {
    blocTest<BlocTestWellMade, Estado>('Tracks preAnalytics correctly',
        build: () {
          TestAnalytic.trackings.clear();
          Bloc.observer = MultiProviderAnalyticsBlocObserver();

          return BlocTestWellMade();
        },
        act: (bloc) => bloc.add(EventoPre('param')),
        expect: () => [Estado(payload: 'onPre'), Estado(payload: 'onPre2')],
        verify: (bloc) => expect(
              TestAnalytic.trackings,
              equals([
                'TestAnalytic: EventoPre:preState:initial',
                'TestAnalytic: EventoPre:param:param',
              ]),
            ));
    blocTest<BlocTestWellMade, Estado>('Tracks postAnalytics correctly',
        build: () {
          Bloc.observer = MultiProviderAnalyticsBlocObserver();
          TestAnalytic.trackings.clear();
          return BlocTestWellMade();
        },
        act: (bloc) => bloc.add(EventoPost('paramCtor')),
        expect: () => [
              SubEstado(
                  payload: 'onPostPayload', subPayload: 'onPostSubPayload'),
              SubEstado(
                  payload: 'onPostPayload2', subPayload: 'onPostSubPayload2')
            ],
        verify: (bloc) => expect(
              TestAnalytic.trackings,
              equals([
                'TestAnalytic: EventoPost:preState:initial',
                'TestAnalytic: EventoPost:paramConstructor:paramCtor',
                'TestAnalytic: EventoPost:paramInBloc:paramInBloc',
                'TestAnalytic: EventoPost:postState:onPostPayload:onPostSubPayload',
              ]),
            ));
    test('Fails on missing inBlocSetUp', () async {
      late Object actualError;
      final completer = Completer<void>();
      final expectation = isA<InBlocCallMissing>()
          .having(
              (e) => e.message,
              'contains the BLoC name in the error message',
              contains('BlocTestWithMissingInBlocSetUpCall'))
          .having(
              (e) => e.message,
              'contains the Event name in the error message',
              contains('EventoPost'))
          .having(
              (e) => e.message,
              'contains bloc\'s event handler filename and line number',
              contains('_onPost'));
      await runZonedGuarded(() async {
        testBloc<BlocTestWithMissingInBlocSetUpCall, Estado>(
            build: () {
              Bloc.observer = MultiProviderAnalyticsBlocObserver();
              TestAnalytic.trackings.clear();
              return BlocTestWithMissingInBlocSetUpCall();
            },
            act: (bloc) => bloc.add(EventoPost('paramCtor')),
            errors: () => [expectation]);
        await completer.future;
      }, (Object error, _) {
        actualError = error;
        if (!completer.isCompleted) completer.complete();
      });
      expect(actualError, expectation);
    });
    test('Fails on missing late field definition in inBlocSetUp', () async {
      late Object actualError;
      final completer = Completer<void>();
      final expectation = isA<AnalyticsPostBlocEventUninitializedFields>()
          .having(
              (e) => e.message,
              'contains the BLoC name in the error message',
              contains('BlocTestWithMissingInBlocSetUpParameters'))
          .having(
              (e) => e.message,
              'contains the Event name in the error message',
              contains('EventoPost'))
          .having((e) => e.message, 'contains the field name',
              contains('paramInBloc'))
          .having(
              (e) => e.message,
              'contains bloc\'s event handler filename and line number',
              contains('_onPost'));
      await runZonedGuarded(() async {
        testBloc<BlocTestWithMissingInBlocSetUpParameters, Estado>(
            build: () {
              Bloc.observer = MultiProviderAnalyticsBlocObserver();
              TestAnalytic.trackings.clear();
              return BlocTestWithMissingInBlocSetUpParameters();
            },
            act: (bloc) => bloc.add(EventoPost('paramCtor')),
            errors: () => [expectation]);
        await completer.future;
      }, (Object error, _) {
        actualError = error;
        if (!completer.isCompleted) completer.complete();
      });
      expect(actualError, expectation);
    });
  });
}
