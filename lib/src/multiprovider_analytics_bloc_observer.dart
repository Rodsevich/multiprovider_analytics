import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:multiprovider_analytics/src/exceptions.dart';
import 'package:multiprovider_analytics/src/models/analytics_bloc_event.dart';

/// {@template MultiProvider_analytics_bloc_observer}
/// A [BlocObserver] which observes all [Bloc] state changes and
/// tracks analytics events.
/// {@endtemplate}
class MultiProviderAnalyticsBlocObserver extends BlocObserver {
  MultiProviderAnalyticsBlocObserver();
  @override
  void onEvent(Bloc<dynamic, dynamic> bloc, Object? event) {
    try {
      if (event is AnalyticsPreBlocEvent) {
        for (final analytic in event.preAnalytics(bloc.state)) {
          analytic.trackEvent();
        }
      }
    } catch (e) {
      //Avisar q tiene q definir el setUp en el BLoC para q funcione
    } finally {
      super.onEvent(bloc, event);
    }
  }

  Object? _lastEvent; //to avoid double tracking

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    final event = transition.event;
    //ensure events are processed just once
    if (event == _lastEvent) {
      return;
    } else {
      _lastEvent = event;
    }
    if (event is AnalyticsPostBlocEvent) {
      if (false == event.hasBeenSetup) {
        throw InBlocCallMissing(bloc, event, StackTrace.current);
      }
      runZonedGuarded(() {
        for (final analytic in event.postAnalytics(
            transition.currentState, transition.nextState)) {
          analytic.trackEvent();
        }
      }, (e, st) {
        if (e.toString().contains('has not been initialized.')) {
          throw AnalyticsPostBlocEventUninitializedFields(e, bloc, event, st);
        }
        throw e;
      });
    }
  }
}
