/// {@template analytics_page}
/// Interface that needs to be used on every page to track analytics.
///  It is used to define the name of the page to be tracked.
///
/// Example:
/// ```dart
/// class HomePage extends StatelessWidget with AnalyticsPage {
///  @override
/// String get analyticsPageName => 'Home';
/// }
/// ```
/// {@endtemplate}
abstract mixin class AnalyticsPage {
  /// The name of the page to be tracked.
  String get analyticsPageName;
}
