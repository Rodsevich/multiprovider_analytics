import 'dart:async';

/// {@template analytic}
/// Defines the structure of an analytic event.
/// {@endtemplate}
abstract class Analytic {
  /// Reports to the analytics service that an event has occurred.
  FutureOr<void> trackEvent();

  String toJson();
}
