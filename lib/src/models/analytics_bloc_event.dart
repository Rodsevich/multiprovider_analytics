import 'dart:async';

import 'package:multiprovider_analytics/src/models/analytic.dart';
import 'package:meta/meta.dart';

/// {@template analytics_pre_bloc_event}
/// Defines de structure of analytics on a bloc event. Always prefer this mixin
/// {@endtemplate}
abstract mixin class AnalyticsPreBlocEvent<T> {
  /// The list of analytics to be tracked.
  /// The `preState` parameter is the current state of the bloc at the time of
  /// the event being received by the bloc.
  List<Analytic> preAnalytics(covariant T preState);
}

/// {@template analytics_post_bloc_event}
/// Defines the necessary for handling analytics after a bloc event has been
/// processed by a BLoC. Use this mixin when the data you should track must be
/// provided dynamically (by processing something in the BLoC)
/// {@endtemplate}
abstract mixin class AnalyticsPostBlocEvent<T> {
  bool hasBeenSetup = false;

  @mustCallSuper
  FutureOr<void> inBlocSetUp() {
    hasBeenSetup = true;
  }

  /// The list of analytics to be tracked.
  /// The `preState` parameter is the current state of the bloc at the time of
  /// a new state being emitted.
  /// The `postState` parameter is the **new state** of the bloc just before
  /// being emitted.
  List<Analytic> postAnalytics(covariant T preState, covariant T postState);
}
