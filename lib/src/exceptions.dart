import 'package:bloc/bloc.dart';
import 'package:multiprovider_analytics/src/models/analytics_bloc_event.dart';

/// {@template InBlocCallMissing}
/// Exception thrown when the [AnalyticsPostBlocEvent] has not been set up properly
/// in the BLoC handler.
/// {@endtemplate}
class InBlocCallMissing extends StateError {
  /// {@macro InBlocCallMissing}
  InBlocCallMissing(
      Bloc bloc, AnalyticsPostBlocEvent event, StackTrace stackTrace)
      : super('You must call event.inBlocSetUp() in the '
            "${bloc.runtimeType} BLoC handler for '${event.runtimeType}' events."
            '\nPertinent file: ${stackTrace.toString().split('\n')[4].split('  ').last}');
}

/// {@template AnalyticsPostBlocEventUninitializedFields}
/// Exception thrown when the [AnalyticsPostBlocEvent] has not been initialized
/// properly in the BLoC handler.
/// {@endtemplate}
class AnalyticsPostBlocEventUninitializedFields extends StateError {
  AnalyticsPostBlocEventUninitializedFields(Object error, Bloc bloc,
      AnalyticsPostBlocEvent event, StackTrace stackTrace)
      : super(
            'You must initialize all the final fields in the event.inBlocSetUp() call of the'
            " ${bloc.runtimeType} BLoC handler for '${event.runtimeType}' events."
            '\nPertinent field: ${error.toString().split("'")[1]}'
            '\nPertinent file: ${stackTrace.toString().split('\n')[11].split('  ').last}');
}

class UnimplementedInjectableType extends UnimplementedError {
  UnimplementedInjectableType(Type type)
      : super('The type $type is not implemented in the MultiProvider.');
}
